#!/usr/bin/env python

import moviepy.editor as mp

class ExtractAudioInVideoCommand(object):

    def __init__(self, args):
        self.video_path = args.get('video_path')
        self.audio_path = args.get('audio_path')
        

    def execute(self):
        video = mp.VideoFileClip(self.video_path)
        video.audio.write_audiofile(self.audio_path)