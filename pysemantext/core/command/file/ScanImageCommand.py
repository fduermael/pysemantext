#!/usr/bin/env python
""" generated source for module ScanImageCommand """
from __future__ import print_function
from pyScanLib import pyScanLib

class ScanImageCommand(object):

    def __init__(self, args):
        self.path = args.get('path', 'scanImage.jpg')
        self.resolution = args.get('dpi', 150)
        self.mode = args.get('mode', 'gray')
        self.scanner = args.get('scanner', None)


    def execute(self):
        scanLib = pyScanLib()

        try:
            scanLib.setScanner(self.scanner)
        except:
            try:
                #sane
                scanners = scanLib.getAllScanners()
                found = False
                print('Available scanners:', scanners)
                for scanner in scanners:
                    print(scanner)
                    if scanner[0].startswith(self.scanner):
                        scanLib.setScanner(scanner[0])
                        found = True
                if not found and scanners:
                    scanLib.setScanner(scanners[0][0])
            except:
                #twain
                scanners = scanLib.getScanners()
                print ('First scanner:', scanners)
                if scanners:
                    scanLib.setScanner(scanners[0])
                else:
                    raise Exception('no scanner found')


        scanLib.setDPI(self.resolution)

        scanLib.setScanArea(left=0, top=0, width=8.267, height=11.693) #(left,top,width,height)
        scanLib.setPixelType(self.mode)  # bw/gray/color
        #
        pil = scanLib.scan()
        pil.save(self.path)
        scanLib.closeScanner()  # unselect selected scanner in setScanners()
        scanLib.close()  # Destory whole class


#main
ScanImageCommand({"path": "/tmp/image_test.jpg", "scanner": "plustek", "mode": "color"}).execute()