#!/usr/bin/env python
from __future__ import print_function
import gkeepapi


class ExtractNoteInGoogleKeepCommand(object):

    def __init__(self, args):
        self.user = args.get('user')
        self.password = args.get('password')
        self.labels = args.get('labels',[])
        self.keep = gkeepapi.Keep()
        self.keep.login(self.user, self.password)
        self.all_labels = self.keep.labels()

    def list_notes_label_text(self, label_text):
        list_notes= self.keep.find(labels=[self.keep.findLabel(label_text)])
        #list_notes = self.keep.get(label_text)
        return list_notes

    def list_notes_label(self, label):
        list_notes= self.keep.find(labels=[label])
        #list_notes = self.keep.get(label_text)
        return list_notes        

    def execute(self):
        result = {}
        if (len(self.labels) != 0):
            for label_text in self.labels:
                notes = self.list_notes_label_text(label_text)
                list = []                
                for n in notes:
                    list.append({"title":n.title,"text":n.text})
                result[label_text] = list    
        else:
            for label in self.all_labels:
                notes = self.list_notes_label(label)
                list = []
                for n in notes:
                    list.append({"title":n.title,"text":n.text})    
                result[label.name] = list        
        return result


