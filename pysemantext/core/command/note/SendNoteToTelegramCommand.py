#!/usr/bin/env python
from __future__ import print_function
from telethon.sync import TelegramClient
from telethon.tl.types import InputPeerUser, InputPeerChannel
from telethon import TelegramClient, sync, events



class SendNoteToTelegramCommand(object):

    def __init__(self, args):
        self.api_id = args.get('api_id')
        self.api_hash = args.get('api_hash')
        self.phone = args.get('phone')
        self.user_phone = args.get('user_phone')
        self.user_messages = args.get('user_messages')

        # creating a telegram session and assigning
        # it to a variable client
        self.client = TelegramClient('session', self.api_id, self.api_hash)
        self.client.connect()

        # in case of script ran first time it will
        # ask either to input token or otp sent to
        # number or sent or your telegram id
        if not self.client.is_user_authorized():
        
            self.client.send_code_request(self.phone)
            
            # signing in the client
            self.client.sign_in(self.phone, input('Enter the code: '))
    

    def execute(self):
        try:
            # receiver user_id and access_hash, use
            # my user_id and access_hash for reference
            #receiver = InputPeerUser(args.get('user_id'), args.get('user_hash'))
            receiver = self.client.get_input_entity(args.get('user_phone'))
 
            # sending message using telegram client
            for user_message in self.user_messages:
                self.client.send_message(receiver, user_message, parse_mode='html')
        except Exception as e:
            
            # there may be many error coming in while like peer
            # error, wwrong access_hash, flood_error, etc
            print(e)


    def finalize(self):
        self.client.disconnect()


