#!/usr/bin/env python

class CleanTextCommand(object):

    def __init__(self, args):
        self.input = args
        self.lang = args.get('lang', 'fr_FR')

    def execute(self):
        output = self.input
        output['name'] = self.input['raw'].lower()
        
        return output


#main
import sys, json, base64
data = json.load(sys.stdin)
#print(json.dumps(data))
result = CleanTextCommand(data).execute()
print(json.dumps(result))