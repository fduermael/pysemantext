import speech_recognition as sr
import os
from os import path
from pydub import AudioSegment


class ExtractTextInAudioCommand(object):

    def __init__(self, args):
        self.path = args.get('path', 'transcript.mp3')
        self.lang = args.get('lang', 'fr_FR')
        self.engine = args.get('engine', 'google')
        self.grammar = args.get('grammar', None)
        self.keyword_entries = args.get('keyword_entries', None)
        self.delete_on_exit = args.get('delete_on_exit', True)

    def execute(self):
        #print ("Reading sound:"+self.path)

        # convert input file to wav                                                       
        sound = AudioSegment.from_file(self.path)
        wav_path = self.path+".wav"
        sound.export(wav_path, format="wav")

        # use the audio file as the audio source                                        
        r = sr.Recognizer()
        with sr.AudioFile(wav_path) as source:
            audio = r.record(source)  # read the entire audio file                  

            # recognize audio depending ot the chosen engine
            if (self.engine == 'google'):    
                text = r.recognize_google(audio, language=self.lang)
            #elif (self.engine == 'bing'):   
            #    text = r.recognize_bing(audio, language=self.lang)    
            #elif (self.engine == 'google_cloud'):   
            #    text = r.recognize_google_cloud(audio, language=self.lang)    
            elif (self.engine == 'sphinx'):   
                text = r.recognize_sphinx(audio_data=audio, grammar=self.grammar, language=self.lang, keyword_entries=self.keyword_entries)
            #    text = r.recognize_houndify(audio, language=self.lang)   
            #elif (self.engine == 'ibm'):   
            #    text = r.recognize_ibm(audio, language=self.lang)       
            #elif (self.engine == 'wit'):   
            #    text = r.recognize_wit(audio)       
            else:
                raise "Unknown engine"

            #if (self.delete_on_exit):
            #    os.remove(wav_path)
                
            return text
