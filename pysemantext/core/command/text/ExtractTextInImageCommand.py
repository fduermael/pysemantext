#!/usr/bin/env python
from __future__ import print_function
try:    
    from PIL import Image
except ImportError:
     import Image

import pytesseract
import numpy as np
import cv2


class ExtractTextInImageCommand(object):

    def __init__(self, args):
        self.path = args.get('path', 'scanImage.jpg')
        self.lang = args.get('lang', 'fr_FR')

    def execute(self):
        img = np.array(Image.open(self.path))
        norm_img = np.zeros((img.shape[0], img.shape[1]))
        img = cv2.normalize(img, norm_img, 0, 255, cv2.NORM_MINMAX)
        img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)[1]
        img = cv2.GaussianBlur(img, (1, 1), 0)
        l = None
        if self.lang== 'fr' or self.lang.startswith('fr_'):
            l = 'fra'
        if self.lang == 'en' or self.lang.startswith('en_'):
            l = 'eng'
        if l is None:
            text = pytesseract.image_to_string(img)
        else:
            text = pytesseract.image_to_string(img, lang=l)
        # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
        # return filter(lambda x: len(x) > 1, text.split('\n\n'))
        return text


