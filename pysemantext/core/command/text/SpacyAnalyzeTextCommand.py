#!/usr/bin/env python
import spacy
import json
import os
import uuid
from uuid import UUID

from spacy.symbols import ENT_TYPE, TAG, DEP
import spacy.util


def uuid_convert(o):
        if isinstance(o, UUID):
            return str(o)

model_names = {}
#model_names['fr']='fr_core_news_md'
#model_names['en']='en_core_web_md'
model_names['fr']='fr_core_news_sm'
model_names['en']='en_core_web_sm'

_models = {}

def get_model(model_name):
    if model_name not in _models:
        _models[model_name] = spacy.load(model_names.get(model_name))
    return _models[model_name]

class SentencesDependencies(object):
    def __init__(self, nlp, text, collapse_punctuation, collapse_phrases):

        self.doc = nlp(text)
        self.model = nlp

        if collapse_punctuation:
            spans = []
            for word in self.doc[:-1]:
                if word.is_punct:
                    continue
                if not word.nbor(1).is_punct:
                    continue
                start = word.i
                end = word.i + 1
                while end < len(self.doc) and self.doc[end].is_punct:
                    end += 1
                span = self.doc[start: end]
                spans.append(
                    (span.start_char, span.end_char, word.tag_, word.lemma_, word.ent_type_)
                )
            for span_props in spans:
                self.doc.merge(*span_props)

        if collapse_phrases:
            for np in list(self.doc.noun_chunks):
                np.merge(np.root.tag_, np.root.lemma_, np.root.ent_type_)


    # def get_index_map(self, content_list):
    #     ''' map token index to index in content_list '''
    #     map = {}
    #     list_index =0
    #     for element in content_list:
    #         map[element['metadata']['index']]= list_index
    #         list_index = list_index + 1
    #     return map    

    # def get_roots(self, content_list):
    #     ''' get all roots '''
    #     roots = []
    #     for element in content_list:
    #         function = element['metadata']['relation']
    #         if (function == 'ROOT'):
    #             roots.append(element)
    #     return roots


    # def build_dependency(self, content_list, map, head): 
    #     for text in content_list:
    #         head_text_token_index = text['metadata']['head_index']
    #         head_text_index = map.get(head_text_token_index)
    #         if head_text_index:
    #             del map[head_text_token_index]
    #             head_text = content_list[head_text_index]
    #             if (head == head_text):
    #                 text['headMap'][str(head_text['id'])]={'relation':text['metadata']['function'],'weight':1.0}
    #                 #head_text['dependencyList'].append(text);   
    #                 head_text['dependencyList'].append({id: text['id']});   
    #                 self.build_dependency(content_list,  map, text)
                 
        


    def to_json(self):
        sents = []
        for sent in self.doc.sents:
            component_list = [{'id':uuid.uuid1(), 'lang': self.model.lang, 'raw': t.text, 'name': t.lemma_, 'type':'token', 
            'metadata' :{'sem' :{ 'ent': { 'type': t.ent_type_, 'iob': t.ent_iob_}}, 'tag': t.tag_, 'index':t.i,
            'dep':{'head':{'relation': t.dep_,'index':t.head.i}}} } for t in sent]
            for component in component_list:
                tag = component['metadata']['tag'].split('__')
                if (tag and len(tag)==2):
                    component['metadata']['pos']=tag[0]
                    component['metadata']['feat']={}
                    for attr_val in tag[1].split('|'):
                        feature = attr_val.split('=')  
                        if (feature and len(feature)==2):                  
                            component['metadata']['feat'][feature[0]]=feature[1]
                else:
                    component['metadata']['pos']=component['metadata']['tag']
                del component['metadata']['tag']
            # remove  'headMap':{}, 'dependencyList':[]
            #map = self.get_index_map(content_list)
            #roots = self.get_roots(content_list)    
            #for root in roots:
            #    self.build_dependency(content_list, map, root)
            sents.append({  'type': 'sentence',
                            'raw': sent.string.strip(),
                            'lang': self.model.lang,
                            'componentList': component_list
                        })
        return sents                
                       
        
class NamedEntities(object):
    def __init__(self, nlp, text):

        self.doc = nlp(text)
        self.model = nlp


    def to_json(self):
        entities=[]
        for ent in self.doc.ents:         
            entities.append({'name':ent.text,'index':ent.start_char,'length':ent.end_char-ent.start_char,'type':ent.label_})       
        return entities
class SpacyAnalyzeTextCommand(object):
    """Returns sentences and dependency parses"""

    def __init__(self, args):
        self.input = args
        self.lang = args.get('lang', 'fr')

    def execute(self):

        raw = self.input['raw']
        model_name = self.lang
        collapse_punctuation =  False
        collapse_phrases = False

        try:
            model = get_model(model_name)
            model.lang = self.lang
            sentences = SentencesDependencies(model,
                                              raw,
                                              collapse_punctuation=collapse_punctuation,
                                              collapse_phrases=collapse_phrases)
            self.input["componentList"] = sentences.to_json()
            entities = NamedEntities(model,raw)
            self.input["metadata"]={}
            self.input["metadata"]["sem"] = {}
            self.input["metadata"]["sem"]["ent"] = entities.to_json()
            return self.input
        except Exception as e:
            raise Exception(
                'Sentence tokenization and Dependency parsing failed',
                '{}'.format(e))


