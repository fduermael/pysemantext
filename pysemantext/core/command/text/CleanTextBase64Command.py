#!/usr/bin/env python

class CleanTextCommand(object):

    def __init__(self, args):
        self.input = args
        self.lang = args.get('lang', 'fr_FR')

    def execute(self):
        output = self.input
        output['name'] = self.input['raw'].lower()
        
        return output


#main
import sys, json, base64

base64_string = sys.argv[1]
base64_bytes = base64_string.encode('utf-8')
decoded_json_bytes = base64.decodebytes(base64_bytes)
decoded_json_string = decoded_json_bytes.decode('utf-8')

data = json.loads(decoded_json_string)

result = CleanTextCommand(data).execute()
print(json.dumps(result))