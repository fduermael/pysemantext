#!/usr/bin/env python
from __future__ import print_function
from telethon import TelegramClient, events, sync


class TelegramListenNoteServer(object):

    def __init__(self, args):
        self.api_id = args.get('api_id')
        self.api_hash = args.get('api_hash')
        self.channels = args.get('channels')
        self.queue = args.get('queue')
        self.client = TelegramClient('anon', self.api_id, self.api_hash)
        
        @self.client.on(events.NewMessage(chats=self.channels))
        async def my_event_handler(event):
            self.queue.put(event)
            
        
    def run(self):
        self.client.start()
        self.client.run_until_disconnected()


