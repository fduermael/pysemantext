#!/usr/bin/env python

import falcon
from core.command.text.SpacyAnalyzeTextCommand import *
from wsgiref import simple_server

class SentsDepResources(object):
    """Returns sentences and dependency parses"""

    def on_post(self, req, resp):
        req_body = req.bounded_stream.read()
        json_data = json.loads(req_body.decode('utf8'))
        text = json_data.get('raw')
        model_name = json_data.get('lang', 'en')
        collapse_punctuation = json_data.get('collapse_punctuation', False)
        collapse_phrases = json_data.get('collapse_phrases', False)

        try:
            model = get_model(model_name)
            sentences = SentencesDependencies(model,
                                              text,
                                              collapse_punctuation=collapse_punctuation,
                                              collapse_phrases=collapse_phrases)

            resp.body = json.dumps(sentences.to_json(),
                                   sort_keys=True,
                                   indent=2,
                                   default=uuid_convert)
            resp.content_type = 'application/json'
            resp.append_header('Access-Control-Allow-Origin', "*")
            resp.status = falcon.HTTP_200
        except Exception as e:
            raise falcon.HTTPBadRequest(
                'Sentence tokenization and Dependency parsing failed',
                '{}'.format(e))

class EntitiesResources(object):
    """Returns entities parses"""

    def on_post(self, req, resp):
        req_body = req.bounded_stream.read()
        json_data = json.loads(req_body.decode('utf8'))
        text = json_data.get('raw')
        model_name = json_data.get('lang', 'en')
 

        try:
            model = get_model(model_name)
            entities = NamedEntities(model,
                                              text)

            resp.body = json.dumps(entities.to_json(),
                                   sort_keys=True,
                                   indent=2)
            resp.content_type = 'application/json'
            resp.append_header('Access-Control-Allow-Origin', "*")
            resp.status = falcon.HTTP_200
        except Exception as e:
            raise falcon.HTTPBadRequest(
                'Entities parsing failed',
                '{}'.format(e))



def run():
    print("Loading spaCy models...")
    for m in model_names.keys():
        get_model(m)
    APP = falcon.API()
    APP.add_route('/sents', SentsDepResources())
    APP.add_route('/entities', EntitiesResources())
    print("Starting spaCy HTTP server...")
    httpd = simple_server.make_server('0.0.0.0', 8000, APP)
    httpd.serve_forever()

