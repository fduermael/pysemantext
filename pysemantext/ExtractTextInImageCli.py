#main
import sys
import platform
from core.command.text.ExtractTextInImageCommand import *
#filepath="/_DVT/datasets/semantext/images/Objectifs_formation_DevOps.JPG"
#filepath="/_DVT/datasets/semantext/images/CI_FDuermael.JPG"
filepath = sys.argv[1]
if platform.system() == 'Linux':
    filepath='/mnt/c'+filepath
else:
    filepath='C:'+filepath    
from datetime import datetime
t1 = datetime.now()    
text = ExtractTextInImageCommand({"path": filepath, "lang": "fr_FR"}).execute();    
t2 = datetime.now();
print(text)
#print("\nbefore:")
#print(t1.time())
#print("\nafter:")
#print(t2.time())
#print("\ntime:")
#print(t2-t1)