#main
import sys
import json
from core.command.note.ExtractNoteInGoogleKeepCommand import *
#filepath="/_DVT/datasets/semantext/images/Objectifs_formation_DevOps.JPG"
#filepath="/_DVT/datasets/semantext/images/CI_FDuermael.JPG"
from datetime import datetime
t1 = datetime.now()    

user = sys.argv[1]
password = sys.argv[2]

if len(sys.argv)==3:
    dct = ExtractNoteInGoogleKeepCommand({"user": user, "password": password}).execute();    
else:
    labels = sys.argv[3]
    dct = ExtractNoteInGoogleKeepCommand({"user": user, "password": password, "labels": labels.split(',')}).execute();    

t2 = datetime.now()

text=json.dumps(dct,indent=2)
#with open('/tmp/keep.json', 'w') as f:
#    f.write(text)
#sys.stdout.write(text)
print(text)
#print("\nbefore:")
#print(t1.time())
#print("\nafter:")
#print(t2.time())
#print("\ntime:")
#print(t2-t1)