#main
import sys
import platform
from core.command.text.ExtractTextInAudioCommand import *
filepath='/Users/Utilisateur/Documents/Enregistrements audio/Enregistrement (2).m4a'
#filepath='/tmp/gen1.mp3'
#filepath = sys.argv[1]
if platform.system() == 'Linux':
    filepath='/mnt/c'+filepath
else:
    filepath='C:'+filepath    
from datetime import datetime
t1 = datetime.now()    
text = ExtractTextInAudioCommand({"path": filepath, "lang": "fr", "engine": "google"}).execute();    
t2 = datetime.now();
print(text)
#print("\nbefore:")
#print(t1.time())
#print("\nafter:")
#print(t2.time())
#print("\ntime:")
#print(t2-t1)