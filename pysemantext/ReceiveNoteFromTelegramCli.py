import threading, queue
import sys
import TelegramListenNoteServer
from core.util.properties import *

#https://docs.python.org/3/library/queue.html
q = queue.Queue()

def worker():
    #launch SpaCy server here
    while True:
        item = q.get()
        print(f'Working on {item}')
        #sentiment analysis of item here
        print(f'Finished {item}')
        q.task_done()

# turn-on the worker thread
threading.Thread(target=worker, daemon=True).start()


def producer():
    channels = ['COVID France']
    if (len(sys.argv)==2):
        channels=sys.argv[1].split(',')
    print (f'Listening on Telegram channels {channels}')
    #properties with api_id and api_hash
    #conf = load_home_properties('telegram_credentials.properties')
    conf = load_properties('c:/Users/Utilisateur/telegram_credentials.properties')

    conf['queue']=q
    conf['channels']=channels
    telegramListenNoteServer = TelegramListenNoteServer(conf)
    telegramListenNoteServer.run()

# turn-on the producer thread
threading.Thread(target=producer, daemon=True).start()
