from setuptools import setup, find_packages
with open('README.md') as f:
    readme = f.read()
with open('LICENSE') as f:
    license = f.read()
setup(
    name='pysemantext',
    version='0.0.1',
    description='python services as helpers for Semantext',
    long_description=readme,
    author='Fabrice DUERMAEL',
    author_email='fab0669@gmail.com',
    url='https://gitlab.com/fduermael/pysemantext',
    license=license,
    packages=find_packages()
)